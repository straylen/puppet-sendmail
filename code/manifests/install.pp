class sendmail::install {
  package {
    [ 'sendmail', 'sendmail-cf', 'm4' ]:
      ensure => present,
  }
}
