  class sendmail::params {
  $smart_host_enable    = hiera('smart_host_enable',true)
  $smart_host           = hiera('smart_host','cern.ch')
  $accept_external_mail = hiera('accept_external_mail',true)
  $user_domain          = hiera('user_domain','mail.cern.ch')
  $local_relay_enable   = hiera('local_relay_enable',true)
  $afs_forward_support  = hiera('afs_forward_support',true)
  $masquerade_enable    = hiera('masquerade_enable',true)
  $root_email           = hiera('root_email',undef)
  $responsible_email    = hiera('mails', [])
  $local_users          = hiera('local_users', [])
  $sendmail_template    = 'sendmail/sendmail.mc.erb'
  $local_users_template = 'sendmail/local-users.erb'
  $forward_template     = 'sendmail/forward.erb'
  #Application related parameters
  $sendmailmc_file      = '/etc/mail/sendmail.mc'
  $sendmailcf_file      = '/etc/mail/sendmail.cf'
  $local_users_file     = '/etc/mail/local-users'
  $forward_file         = '/root/.forward'
  $file_group           = 'root'
  $owner                = 'root'
  $mode                 = '0644'

}
