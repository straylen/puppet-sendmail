class sendmail inherits sendmail::params {

  class {'sendmail::install':}
  class {'sendmail::config':}
  class {'sendmail::service':}
  class {'sendmail::newaliases':}

}
