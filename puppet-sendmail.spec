Name:           puppet-sendmail
Version:        2.0
Release:        1%{?dist}
Summary:        Masterless puppet module for sendmail

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

%description
Masterless puppet module for sendmail.

%prep
%setup -n code

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/sendmail/
cp -ra * %{buildroot}/%{_datadir}/puppet/modules/sendmail/
touch %{buildroot}/%{_datadir}/puppet/modules/sendmail/linuxsupport

%files -n puppet-sendmail
%{_datadir}/puppet/modules/sendmail
%doc README.md

%changelog
* Wed May 02 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- New version for locmap 2.0

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
