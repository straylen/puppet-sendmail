NAME=$(shell basename `git rev-parse --show-toplevel`)
VERSION= $(shell git describe --tags | sed s/v// | cut -d "-" -f 1)
RELEASE= $(shell git describe --tags | sed s/v// | cut -d "-" -f 2)

sources:
	tar -zcvf $(NAME)-$(VERSION).tar.gz code/

build:
	koji build --nowait potd7 git+ssh://git@gitlab.cern.ch:7999/linuxsupport/$(NAME).git#v$(VERSION)-$(RELEASE)

scratch:
	koji build --scratch potd7 git+ssh://git@gitlab.cern.ch:7999/linuxsupport/$(NAME).git#v$(VERSION)-$(RELEASE)
